window.onload = function () {
          var aSound = new Audio();
          aSound.src = "/media/alert.ogg";
          var match = window.location.hash.match(/invite=([a-zA-Z0-9]+)/);
          if (match && confirm('Would you like to connect to the table "'+match[1]+'"?')) {
             invite(match[1]);
          }
};
var player = 0,
    table = false,
    socket = false,
    timeout = false,
    newtable = false,
    opentable = false,
    privetable = false;
function connect (reqtable,open) {
         if (typeof(io) != 'undefined') {
            table = (reqtable?reqtable:randomID(20));
            timeout = setTimeout(function(){
                  msg('Could not connect to RPS server.');
            },5000);
            socket = io.connect('ws://'+window.document.location.host+'/rps');
            socket.on('connect',function () {
                  clearTimeout(timeout);
                  if (opentable == false) {
                     socket.emit('request_table',{table:table});
                   } else {
                     socket.emit('join_open_table',{});
                  }
            });
            socket.on('disconnect',function () {
                  msg("Disconnected from RPS server.");
            });
            socket.on('error',function () {
                  //ignore this
            });
            socket.on('connected_to_table',function(data) {
                  $('chat').innerHTML = '';
                  table = data.table;
                  player = data.player;
                  window.location.hash = "#invite="+table;
                  if (player == 1) {
                     msg("Invite Code: "+table+" (<a href=\"./#invite="+table+"\">Link</a>)");
                     if (privetable) {
                        socket.emit('set_private',{table:table});
                     }
                   } else {
                     msg("You are player #2! Choose your move.");
                  }
            });
            socket.on('winner',function (data) {
                  if (data.p == player) {
                     msg("You win! Choose again.");
                  } else if (data.p == 0) {
                     msg("It was a tie! Choose again.");
                  } else {
                     msg("You lost. Choose again.");
                  }
                  $('P1').innerHTML = data.P1;
                  $('P2').innerHTML = data.P2;
                  $('ties').innerHTML = data.ties;
            });
            socket.on('play_alert', function () {
                  if (!aSound) { 
                     var aSound = new Audio();
                     aSound.src = "/media/alert.ogg";
                  }
                  aSound.play();
                  prependToChat("<strong>Them: <i>Buzz!</i></strong><br />");
            });
            socket.on('played_alert',function (data) {
                  if (data.played == true) {
                     prependToChat('<strong><i>Buzz sent!</i></strong><br />');
                   } else {
                     prependToChat('<strong><i>You can not send more than one buzz every '+data.limit+' seconds!</i></strong><br />');
                  }
            });
            socket.on('player_left',function(data) {
                  msg('Player left, this table is now closed. <a href="javascript:;" onclick="connectToTable();">Create another table?</a>');
                  $('P1').innerHTML = $('P2').innerHTML = $('ties').innerHTML = 0;
            });
            socket.on('table_full',function(data) {
                  msg(data.content.replace(/{{CREATE}}/,'<a href="javascript:;" onclick="connectToTable();">Create your own!</a>'));
            });
            socket.on('fatal_error',function(data) {
                  msg(data.content);
                  socket.disconnect();
            });
            socket.on('table_ready',function () {
                  msg("Player #2 connected! Choose your move.");
            });
            socket.on('chosen',function (data) {
                  msg(data.content);
            });
            socket.on('get_new_table',function () {
                  $('P1').innerHTML = $('P2').innerHTML = $('ties').innerHTML = 0;
                  table = (newtable?newtable:randomID(20));
                  newtable = false;
                  socket.emit('request_table',{table:table});
            });
            socket.on('open_table',function (data) {                  
                  if (data.table !== null && confirm('Would you like to connect to the table "'+data.table+'"?')) {
                     invite(data.table);
                   } else {
                     msg("No open tables.");
                  }
            });
            socket.on('chat-in',function (data) {
                  $('chat').innerHTML = '<strong>Them:</strong> '+data.msg+'<br />'+$('chat').innerHTML;
            });
          } else {
            msg("Could not connect to RPS server.");
         }
         return socket;
}
function play (id) {
         if (socket && socket.socket.connected) {
            if (typeof(chosen) != 'undefined') {
               $(chosen).style.border = '1px solid #ffffff';
            }
            chosen = id;
            $(chosen).style.border = '1px solid #737373';
            socket.emit('chosen',{table:table,move:id});
            msg('Waiting on other player...');
         }
}
function $ (id) {
         return document.getElementById(id);
}
function invite (code) {
         var code = (code?code:prompt("Invite Code",""));
         if (code) {
            code = code.replace(/[^a-zA-Z0-9]/g, "");
            if (code) {
               connectToTable(code);
               return;
            }
         }
         alert("You must enter a real code.");
}
function connectToTable (reqtable) {
         opentable =  privetable = false;
         if (socket) {
            newtable = (reqtable?reqtable:randomID(20));
            socket.emit('new_table',{table:table});
          } else {
            connect(reqtable);
         }
}
function msg (str) {
         $('msg').innerHTML = str;
}
function randomID (L) {
         var s= '';
         var randomchar = function () {
             var n = Math.floor(Math.random()*62);
             if (n < 10) { return n; }
             if (n < 36) { return String.fromCharCode(n+55); }
             return String.fromCharCode(n+61);
         }
         while (s.length < L) { s+= randomchar(); }
         return s;
}
function joinOpenTable () {
         if (socket) {
            socket.emit('leave_table',{table:table});
            socket.emit('join_open_table',{});
          } else  {
            opentable = true;
            connect(null,true);
         }
}
function createPrivate () {
         privetable = true;
         if (socket) {
            newtable = randomID(20);
            socket.emit('new_table',{table:table});
          } else {
            connect(false);
         }
}
function sendToChat () {
         if (socket && table) {
            var txt = $('chatIn').value.toString().replace(/[^a-zA-Z0-9!.?:=\s]/g,"");
            if (txt) {
               prependToChat('<strong>You:</strong> '+txt+'<br />');
               socket.emit('chat-out',{table:table,msg:txt});
            }
         }
         $('chatIn').value = '';
}
function sendAlert () {
         if (socket && table) {
            socket.emit('send-alert',{table:table});
         };
         return false;
};
function prependToChat(txt) {
       $('chat').innerHTML = txt+$('chat').innerHTML
}
