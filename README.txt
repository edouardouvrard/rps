This is just a silly rock/paper/scissors game I made a while ago.

Install socket.io:
  npm install socket.io

Edit index.js to set your port and other settings.

NOTE: I wrote this when I first started using node.js sometime in 2011, so the source is not fantastic.
      I'm not sure when or if I'll ever update this. You can do with this as you please.
